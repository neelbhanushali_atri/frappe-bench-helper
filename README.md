# Bench helper
Author: Neel Bhanushali &lt;neel@atriina.com&gt;<br><br>
`wget https://gitlab.com/neelbhanushali_atri/frappe-bench-helper/-/raw/v1.0.1/helper.sh`<br>
`chmod +x helper.sh`<br>
`./helper.sh <<COMMAND>> <<OPTIONS>>`<br>
<br>
## Commands:<br>
| Command | Description |
| ------ | ------ |
| backup | Spits out backup file's absolute path |
| restore | Needs (-b,--backup-file) input | 
| new-site | Needs (-db,--db-name), --domain-name, (-e,--install-erpnext) input

## Options:<br>
| Option | Description |
| ------ | ------ |
| -h,--help | Show usage |
| -l,--frappe-bench-location | Set frappe-bench directory location. | 
| -s,--site | Set bench site. |
| -b,--backup-file | Set backup file to be restored. |
| -db,--db-name | Set database name for new-site |
| --domain-name | Set domain name for new-site |
| -e,--install-erpnext | Set whether erpnext is supposed to be installed |
