#!/bin/bash

HELP()
{
    cat <<EOF
Bench helper
Author: Neel Bhanushali <neel@atriina.com>
Version: 1.0.1
Usage: ./helper.sh <<COMMAND>> <<OPTIONS>>

Commands:
backup                      Spits out backup sql.gz file's absolute path and files archive absolute path (files archive path is given if -f,--with-files is set) 
restore                     Needs (--database-backup-file, --with-files) input. (--public-files-archive, --private-files-archive needed if --with-files is set) 
new-site                    Needs (-db,--db-name), --domain-name, (-e,--install-erpnext) input

Options:
-h,--help                   Show usage
-l,--frappe-bench-location  Set frappe-bench directory location.
-s,--site                   Set bench site.
--database-backup-file      Set database backup file to be restored.
--public-files-archive      Set public files archive file to be restored.
--private-files-archive     Set private files archive file to be restored.
-db,--db-name               Set database name for new-site
--domain-name               Set domain name for new-site
-e,--install-erpnext        Set whether erpnext is supposed to be installed
-f,--with-files             Set whether backup is supposed to be taken with files

EOF
}

GET_INPUTS()
{
    # take inputs from command line arguments
    while [ $# -gt 0 ]
    do
        case $1 in
            -h | --help)
                HELP
                exit
                ;;

            -l | --frappe-bench-location)
                shift
                BENCH_LOC=$1
                ;;

            -s | --site)
                shift
                SITE=$1
                ;;

            backup | restore | new-site)
                ACTION=$1
                ;;

            --database-backup-file)
                shift
                DATABASE_BACKUP_FILE=$1
                ;;
                
            --public-files-archive)
                shift
                PUBLIC_FILES_ARCHIVE=$1
                ;;
                
            --private-files-archive)
                shift
                PRIVATE_FILES_ARCHIVE=$1
                ;;
                
            -db | --db-name)
                shift
                DB_NAME=$1
                ;;
                
            --domain-name)
                shift
                DOMAIN_NAME=$1
                ;;
                
            -e | --install-erpnext)
                shift
                INSTALL_ERPNEXT=true
                ;;
                
            -f | --with-files)
                shift
                WITH_FILES=true
                ;;
        esac
        shift
    done

    # take inputs from users if not received in command line arguments

    # action
    while [ -z "$ACTION" ]; do
        read -p "Enter command (backup|restore|new-site): " ACTION
        if [[ "$ACTION" != "backup" && "$ACTION" != "restore" && "$ACTION" != "new-site" ]]; then
            unset ACTION
        fi
    done
    
    # frappe-bench location
    if [ ! -d "$BENCH_LOC" ]; then
        unset BENCH_LOC
    fi
    while [ -z "$BENCH_LOC" ]; do
        read -p "Enter frappe-bench location: " BENCH_LOC
        if [ ! -d "$BENCH_LOC" ]; then
            echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
            echo "frappe-bench location not found."
            unset BENCH_LOC
        fi
    done
    
    # site
    if [ "$ACTION" != "new-site" ]; then
        # don't check if folder exists in case of new-site command
        if [ ! -d "$BENCH_LOC/sites/$SITE" ]; then
            unset SITE
        fi
    fi
    while [ -z "$SITE" ]; do
        read -p "Enter site name: " SITE
        if [ "$ACTION" != "new-site" ]; then
            # don't check if folder exists in case of new-site command
            if [ ! -d "$BENCH_LOC/sites/$SITE" ]; then
                echo "xxxxxxxxxxxxxxx"
                echo "Site not found."
                unset SITE
            fi
        fi
    done
    
    if [ "$ACTION" == "backup" ]; then
        # if backup is supposed to be taken with files
        while [ -z "$WITH_FILES" ]; do
            read -p "Do you want to backup the files as well? (Y/n): " WITH_FILES
            if [[ "$WITH_FILES" == "y" || "$WITH_FILES" == "Y" ]]; then
                WITH_FILES=true
            else
                WITH_FILES=false
            fi
        done
    fi
    
    if [ "$ACTION" == "restore" ]; then
        # database backup file path
        if [ ! -f "$DATABASE_BACKUP_FILE" ]; then
            unset BACKUP_FILE
        fi
        while [ -z "$DATABASE_BACKUP_FILE" ]; do
            read -p "Enter database backup file location: " DATABASE_BACKUP_FILE
            if [ ! -f "$DATABASE_BACKUP_FILE" ]; then
                echo "xxxxxxxxxxxxxxxxxxxxxx"
                echo "Database Backup file not found."
                unset BACKUP_FILE
            fi
        done
        
        # set flag that indicates if files are also to be restored
        while [ -z "$WITH_FILES" ]; do
            read -p "Do you want to restore the files as well? (Y/n): " WITH_FILES
            if [[ "$WITH_FILES" == "y" || "$WITH_FILES" == "Y" ]]; then
                WITH_FILES=true
            else
                WITH_FILES=false
            fi
        done
        
        # get files archive if files are to be restored
        if $WITH_FILES; then
            # public files archive
            while [ -z "$PUBLIC_FILES_ARCHIVE" ]; do
                read -p "Enter public backup file location: " PUBLIC_FILES_ARCHIVE
                if [ ! -f "$PUBLIC_FILES_ARCHIVE" ]; then
                    echo "xxxxxxxxxxxxxxxxxxxxxx"
                    echo "Public files archive not found."
                    unset PUBLIC_FILES_ARCHIVE
                fi
            done
            
            # private files archive
            while [ -z "$PRIVATE_FILES_ARCHIVE" ]; do
                read -p "Enter private backup file location: " PRIVATE_FILES_ARCHIVE
                if [ ! -f "$PRIVATE_FILES_ARCHIVE" ]; then
                    echo "xxxxxxxxxxxxxxxxxxxxxx"
                    echo "Private files archive not found."
                    unset PRIVATE_FILES_ARCHIVE
                fi
            done
        fi
    fi
    
    
    if [ "$ACTION" == "new-site" ]; then
        # db name
        while [ -z "$DB_NAME" ]; do
            read -p "Enter database name: " DB_NAME
        done
        
        # domain name
        while [ -z "$DOMAIN_NAME" ]; do
            read -p "Enter domain: " DOMAIN_NAME
        done
        
        # if erpnext app is supposed to be installed
        while [ -z "$INSTALL_ERPNEXT" ]; do
            read -p "Do you want to install ERPNext? (Y/n): " INSTALL_ERPNEXT
            if [[ "$INSTALL_ERPNEXT" == "y" || "$INSTALL_ERPNEXT" == "Y" ]]; then
                INSTALL_ERPNEXT=true
            else
                INSTALL_ERPNEXT=false
            fi
        done
    fi
}

# call get inputs and pass all the command line arguments ($@)
GET_INPUTS $@

# script running begins here

# cd into frappe-bench dir
cd $BENCH_LOC

# run respective scripts as per action
case "$ACTION" in
    backup)
        if $WITH_FILES; then
            bench --site $SITE backup --with-files
        else
            bench --site $SITE backup
        fi
        DATABASE_BACKUP_FILE=$(ls -t sites/$SITE/private/backups/*.sql.gz | head -n1)
        echo
        echo "Database File"
        echo "----"
        echo "$PWD/sites/$SITE/private/backups/$DATABASE_BACKUP_FILE"
        ;;

    restore)
        if $WITH_FILES; then
            bench --site $SITE --force restore $DATABASE_BACKUP_FILE --with-public-files $PUBLIC_FILES_ARCHIVE --with-private-files $PRIVATE_FILES_ARCHIVE
        else
            bench --site $SITE --force restore $DATABASE_BACKUP_FILE
        fi
        ;;
        
    new-site)
        bench new-site $SITE --db-name $DB_NAME
        if $INSTALL_ERPNEXT; then
            bench --site $SITE install-app erpnext
        fi
        bench setup add-domain $DOMAIN_NAME --site $SITE
        sudo bench setup lets-encrypt $SITE --custom-domain $DOMAIN_NAME
        bench --site $SITE migrate
        sudo bench setup production $USER
esac
